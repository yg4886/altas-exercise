package com.yan.codeexercise.common;

public class DataAccessObjectException extends Exception {
    protected String message;


    public DataAccessObjectException(String message) {
        super(message);
        this.message = message;
    }
}

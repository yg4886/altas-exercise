package com.yan.codeexercise.dao.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.yan.codeexercise.common.DataAccessObjectException;
import com.yan.codeexercise.dao.MarketPlaceDaoService;
import com.yan.codeexercise.model.AccountModel;
import com.yan.codeexercise.model.ContactModel;
import net.java.ao.DBParam;
import net.java.ao.Query;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Component
public class MarketPlaceAOService implements MarketPlaceDaoService {

    private static final Logger log = LoggerFactory.getLogger(MarketPlaceAOService.class);

    @ComponentImport
    ActiveObjects ao;

    @Inject
    public MarketPlaceAOService(ActiveObjects ao) {
        this.ao = ao;
    }

    /**
     *  Get a Contact AO entity from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     * @return Contact AO entity
     */
    private Contact getContactEntity(int ID) {

        log.debug("Retrieving Contact by ID " + ID);
        return ao.get(Contact.class, ID);
    }

    /**
     *  Get a Contact model from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    public ContactModel getContact(int ID) throws DataAccessObjectException {
        return cloneContact(getContactEntity(ID));
    }

    /**
     *  Get a Contact model from database by Email
     * @param email Contact's email. Here I consider email is an unique identifier for contact (it is not necessary).
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    public ContactModel getContactByEmail(String email) throws DataAccessObjectException{
        ContactModel model = null;

        Contact[] existings = ao.find(Contact.class, Query.select().where("EMAIL = ?", email));

        if (existings.length > 0) {
            model = cloneContact(existings[0]);
        }

        return model;
    }

    /**
     *  Get all Contact models from database
     * @return A list of Contact Models
     * @throws DataAccessObjectException
     */
    public List<ContactModel> getContacts() throws DataAccessObjectException{
        List<ContactModel> list = new ArrayList<ContactModel>();

        Contact[] contacts = ao.find(Contact.class);
        if (contacts.length > 0) {
            list = cloneContacts(contacts);
        }

        return list;
    }

    /**
     * creates a persisted Contact entity in the database
     * @param model ContactModel
     * @return Contact AO entity
     */
    private Contact createContact(ContactModel model){
        Contact newAccount = ao.create(Contact.class,
                new DBParam("EMAIL", model.getEmail()));

        Account account = null;
        if (model.getAccount() != null) {
            account = getAccountEntity(model.getAccount().getId());
        }
        
        newAccount.setName(model.getName());
        newAccount.setEmail(model.getEmail());
        newAccount.setAddressLine1(model.getAddressLine1());
        newAccount.setAddressLine2(model.getAddressLine2());
        newAccount.setCity(model.getCity());
        newAccount.setState(model.getState());
        newAccount.setCountry(model.getCountry());
        newAccount.setPostalCode(model.getPostalCode());
        newAccount.setAccount(account);

        newAccount.save();

        return newAccount;
    }

    /**
     *  Update or create a persisted Contact record in the database.
     *  You can also use this method to associate a contact to an account
     * @param model  ContactModel
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    public ContactModel saveContact(ContactModel model) throws DataAccessObjectException{
        Contact contact = getContactEntity(model.getId());

        if (contact != null) {
            Account account = null;
            if (model.getAccount() != null) {
                account = getAccountEntity(model.getAccount().getId());
            }

            contact.setName(model.getName());
            contact.setEmail(model.getEmail());
            contact.setAddressLine1(model.getAddressLine1());
            contact.setAddressLine2(model.getAddressLine2());
            contact.setCity(model.getCity());
            contact.setState(model.getState());
            contact.setCountry(model.getCountry());
            contact.setPostalCode(model.getPostalCode());
            contact.setAccount(account);

            contact.save();
        } else {
            createContact(model);
        }

        return model;
    }

    /**
     * delete a single Contact record from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     */
    public void deleteContact(int ID) {
        Contact existing = getContactEntity(ID);
        if (existing != null) {
            ao.delete(existing);
        }
    }

    /**
     * Convert a Contact AO entity to a ContactModel
     * @param contact a Contact AO entity
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    private ContactModel cloneContact(Contact contact) throws DataAccessObjectException {

        if (contact == null) {
            log.warn("Attempting to clone an empty Contact!");
            return null;
        }

        ContactModel model = new ContactModel();
        try {
            BeanUtils.copyProperties(contact, model);

            model.setId(contact.getID());
            if (contact.getAccount() != null) {
                model.setAccount(cloneAccount(contact.getAccount()));
            }
        } catch (Exception e) {
            log.error("Error cloning Contact: ", e);
            throw new DataAccessObjectException("Error cloning Contact: " + e.getMessage());
        }

        return model;
    }

    /**
     * Convert a list of Contact AO entities to a list of ContactModels
     * @param contacts Contact AO entities
     * @return A list of ContactModel
     * @throws DataAccessObjectException
     */
    private List<ContactModel> cloneContacts(Contact[] contacts) throws DataAccessObjectException{
        List<ContactModel> list = new ArrayList<ContactModel>();

        if (contacts.length < 1) {
            return list;
        }

        for (Contact contact : contacts) {
            ContactModel contactModel = cloneContact(contact);
            if (contactModel != null) {
                list.add(contactModel);
            }
        }

        return list;
    }

    /**
     *  Get an Account AO entity from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return Account AO entity
     */
    private Account getAccountEntity(int ID) {

        log.debug("Retrieving Account by ID " + ID);
        return ao.get(Account.class, ID);
    }

    /**
     *  Get an Account model from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return AccountModel
     * @throws DataAccessObjectException
     */
    public AccountModel getAccount(int ID) throws DataAccessObjectException{
        return cloneAccount(getAccountEntity(ID));
    }

    /**
     *  Get an Account model from database by Company Name
     * @param companyName Account's company name. Here I consider company name is an unique identifier for contact (it is not necessary).
     * @return AccountModel
     * @throws DataAccessObjectException
     */
    public AccountModel getAccountByCompanyName(String companyName) throws DataAccessObjectException{
        AccountModel model = null;

        Account[] existings = ao.find(Account.class, Query.select().where("COMPANY_NAME = ?", companyName));

        if (existings.length > 0) {
            model = cloneAccount(existings[0]);
        }

        return model;
    }

    /**
     * Get all Contact models from database
     * @return A list of Account Models
     * @throws DataAccessObjectException
     */
    public List<AccountModel> getAccounts() throws DataAccessObjectException{
        List<AccountModel> list = new ArrayList<AccountModel>();

        Account[] accounts = ao.find(Account.class);
        if (accounts.length > 0) {
            list = cloneAccounts(accounts);
        }

        return list;
    }

    /**
     * creates a persisted Account entity in the database
     * @param model AccountModel
     * @return Account AO entity
     */
    private Account createAccount(AccountModel model) {
        Account newAccount = ao.create(Account.class,
                new DBParam("COMPANY_NAME", model.getCompanyName()));

        newAccount.setCompanyName(model.getCompanyName());
        newAccount.setAddressLine1(model.getAddressLine1());
        newAccount.setAddressLine2(model.getAddressLine2());
        newAccount.setCity(model.getCity());
        newAccount.setState(model.getState());
        newAccount.setCountry(model.getCountry());
        newAccount.setPostalCode(model.getPostalCode());

        newAccount.save();

        return newAccount;
    }

    /**
     *  Update or create a persisted Account record in the database.
     * @param model AccountModel
     * @return AccountModel
     */
    public AccountModel saveAccount(AccountModel model){
        Account account = getAccountEntity(model.getId());

        if (account != null) {

            account.setCompanyName(model.getCompanyName());
            account.setAddressLine1(model.getAddressLine1());
            account.setAddressLine2(model.getAddressLine2());
            account.setCity(model.getCity());
            account.setState(model.getState());
            account.setCountry(model.getCountry());
            account.setPostalCode(model.getPostalCode());

            account.save();
        } else {
            createAccount(model);
        }

        return model;
    }

    /**
     * delete a single Account record from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     */
    public void deleteAccount(int ID) {
        Account existing = getAccountEntity(ID);
        if (existing != null) {

            Contact[] contacts = existing.getContacts();
            if (contacts.length > 0) {
                for (Contact contact : contacts) {
                    contact.setAccount(null);
                    contact.save();
                }
            }

            ao.delete(existing);
        }
    }

    /**
     * Convert an Account AO entity to a AccountModel
     * @param account an account AO entity
     * @return AccountModel
     * @throws DataAccessObjectException
     */
    private AccountModel cloneAccount(Account account) throws DataAccessObjectException {

        if (account == null) {
            log.warn("Attempting to clone an empty Account!");
            return null;
        }

        AccountModel model = new AccountModel();
        try {
            BeanUtils.copyProperties(account, model);
            model.setId(account.getID());
        } catch (Exception e) {
            log.error("Error cloning Account: ", e);
            throw new DataAccessObjectException("Error cloning Account: " + e.getMessage());
        }

        return model;
    }

    /**
     * Convert a list of Account AO entities to a list of AccountModels
     * @param accounts Account AO entities
     * @return A list of AccountModels
     * @throws DataAccessObjectException
     */
    private List<AccountModel> cloneAccounts(Account[] accounts) throws DataAccessObjectException{
        List<AccountModel> list = new ArrayList<AccountModel>();

        if (accounts.length < 1) {
            return list;
        }

        for (Account account : accounts) {
            AccountModel accountModel = cloneAccount(account);
            if (accountModel != null) {
                list.add(accountModel);
            }
        }

        return list;
    }

    /**
     * Get all contacts for an account by account PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return A list of ContactModels
     * @throws DataAccessObjectException
     */
    public List<ContactModel> getContactsForAccount(int ID) throws DataAccessObjectException {
        List<ContactModel> list = new ArrayList<ContactModel>();

        Account account = getAccountEntity(ID);
        if (account.getContacts().length > 0) {
           list = cloneContacts(account.getContacts());
        }
        return list;
    }
}

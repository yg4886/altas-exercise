package com.yan.codeexercise.dao.ao;

import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;

public interface Account extends Entity{

    @NotNull
    String getCompanyName();
    void setCompanyName(String companyName);

    @StringLength(StringLength.UNLIMITED)
    String getAddressLine1();
    void setAddressLine1(String addressLine1);

    @StringLength(StringLength.UNLIMITED)
    String getAddressLine2();
    void setAddressLine2(String addressLine2);

    String getCity();
    void setCity(String city);

    String getState();
    void setState(String state);

    String getPostalCode();
    void setPostalCode(String postalCode);

    String getCountry();
    void setCountry(String country);

    @OneToMany
    public Contact[] getContacts();
}

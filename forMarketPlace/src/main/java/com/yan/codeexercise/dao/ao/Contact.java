package com.yan.codeexercise.dao.ao;

import net.java.ao.Entity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;

public interface Contact extends Entity {

    String getName();
    void setName(String name);

    @NotNull
    String getEmail();
    void setEmail(String email);

    @StringLength(StringLength.UNLIMITED)
    String getAddressLine1();
    void setAddressLine1(String addressLine1);

    @StringLength(StringLength.UNLIMITED)
    String getAddressLine2();
    void setAddressLine2(String addressLine2);

    String getCity();
    void setCity(String city);

    String getState();
    void setState(String state);

    String getPostalCode();
    void setPostalCode(String postalCode);

    String getCountry();
    void setCountry(String country);

    Account getAccount();
    void setAccount(Account account);
}

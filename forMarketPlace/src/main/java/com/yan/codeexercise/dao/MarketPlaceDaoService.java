package com.yan.codeexercise.dao;

import com.yan.codeexercise.common.DataAccessObjectException;
import com.yan.codeexercise.dao.ao.Account;
import com.yan.codeexercise.model.AccountModel;
import com.yan.codeexercise.model.ContactModel;
import java.util.List;

public interface MarketPlaceDaoService {

    /**
     *  Get a Contact model from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    ContactModel getContact(int ID) throws DataAccessObjectException;

    /**
     *  Get a Contact model from database by Email
     * @param email Contact's email. Here I consider email is an unique identifier for contact (it is not necessary).
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    ContactModel getContactByEmail(String email) throws DataAccessObjectException;

    /**
     *  Get all Contact models from database
     * @return A list of Contact Models
     * @throws DataAccessObjectException
     */
    List<ContactModel> getContacts() throws DataAccessObjectException;


    /**
     *  Update or create a persisted Contact record in the database.
     *  You can also use this method to associate a contact to an account
     * @param model  ContactModel
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    ContactModel saveContact(ContactModel model) throws DataAccessObjectException;

    /**
     * delete a single Contact record from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     */
    void deleteContact(int ID);

    /**
     *  Get an Account model from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return AccountModel
     * @throws DataAccessObjectException
     */
    AccountModel getAccount(int ID) throws DataAccessObjectException;

    /**
     *  Get an Account model from database by Company Name
     * @param companyName Account's company name. Here I consider company name is an unique identifier for contact (it is not necessary).
     * @return AccountModel
     * @throws DataAccessObjectException
     */
    AccountModel getAccountByCompanyName(String companyName) throws DataAccessObjectException;

    /**
     * Get all Contact models from database
     * @return A list of Account Models
     * @throws DataAccessObjectException
     */
    List<AccountModel> getAccounts() throws DataAccessObjectException;

    /**
     *  Update or create a persisted Account record in the database.
     * @param model AccountModel
     * @return AccountModel
     */
    AccountModel saveAccount(AccountModel model);

    /**
     * delete a single Account record from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     */
    void deleteAccount(int ID);

    /**
     * Get all contacts for an account by account PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return A list of ContactModels
     * @throws DataAccessObjectException
     */
    List<ContactModel> getContactsForAccount(int ID) throws DataAccessObjectException;


}

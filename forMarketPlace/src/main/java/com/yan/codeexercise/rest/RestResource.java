package com.yan.codeexercise.rest;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.google.gson.Gson;
import com.yan.codeexercise.api.MarketPlaceService;
import com.yan.codeexercise.impl.MarketPlaceServiceImpl;
import com.yan.codeexercise.model.AccountModel;
import com.yan.codeexercise.model.ContactModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Scanned
@Path("/common")
public class RestResource {

    private static final Logger log = LoggerFactory.getLogger(RestResource.class);

    private MarketPlaceService marketPlaceService;

    public RestResource(MarketPlaceServiceImpl marketPlaceService) {
        this.marketPlaceService = marketPlaceService;
    }

    @GET
    @Path("health")
    public Response getHealthCheck(){
        return Response.ok("{ 'status': 'ok!@!' }").build();
    }

    /**
     *  Get a Contact model from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     * @return ContactModel in json
     */
    @GET
    @Path("contact/id/{ID}")
    public Response getContactByID(@PathParam("ID") int ID) {
        ContactModel model = null;

        try {
            model = marketPlaceService.getContact(ID);
        } catch (Exception e) {
            String errMsg = "Error retrieving Contact with ID " + ID + ". " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok(new Gson().toJson(model)).build();
    }

    /**
     *  Get a Contact model from database by Email
     * @param email Contact's email. Here I consider email is an unique identifier for contact (it is not necessary).
     * @return ContactModel in json
     */
    @GET
    @Path("contact/email/{email}")
    public Response getContactByEmail(@PathParam("email") String email) {
        ContactModel model = null;

        try {
            model = marketPlaceService.getContactByEmail(email);
        } catch (Exception e) {
            String errMsg = "Error retrieving Contact with email " + email + ". " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok(new Gson().toJson(model)).build();
    }

    /**
     *  Get all Contact models from database
     * @return A list of Contact Models in json
     */
    @GET
    @Path("contact/all")
    public Response getAllContacts() {
        List<ContactModel> list = new ArrayList<ContactModel>();

        try {
            list = marketPlaceService.getContacts();
        } catch (Exception e) {
            String errMsg = "Error retrieving all Contacts. " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok(new Gson().toJson(list)).build();
    }

    /**
     * Update or create a persisted Contact record in the database.
     * You can also use this endpoint to associate a contact to an account
     * @param model  ContactModel
     * @return
     */
    @POST
    @Path("contact/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveContact(ContactModel model){

        try {
            marketPlaceService.saveContact(model);
        }catch (Exception e){
            String errMsg = "Error saving/updating Contact. " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok("Contact added/updated").build();
    }

    /**
     * Delete a single Contact record from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     */
    @DELETE
    @Path("contact/delete/{ID}")
    public Response deleteContact(@PathParam("ID") int ID){

        try{
            marketPlaceService.deleteContact(ID);
        } catch (Exception e){
            String errMsg = "Error deleting Contact. " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok("Contact deleted").build();
    }

    /**
     *  Get an Account model from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return AccountModel in json
     */
    @GET
    @Path("account/id/{ID}")
    public Response getAccountByID(@PathParam("ID") int ID) {
        AccountModel model = null;

        try {
            model = marketPlaceService.getAccount(ID);
        } catch (Exception e) {
            String errMsg = "Error retrieving Account with ID " + ID + ". " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok(new Gson().toJson(model)).build();
    }

    /**
     *  Get an Account model from database by Company Name
     * @param comName Account's company name. Here I consider company name is an unique identifier for contact (it is not necessary).
     * @return AccountModel in json
     */
    @GET
    @Path("account/comName/{comName}")
    public Response getAccountByEmail(@PathParam("comName") String comName) {
        AccountModel model = null;

        try {
            model = marketPlaceService.getAccountByCompanyName(comName);
        } catch (Exception e) {
            String errMsg = "Error retrieving account with company name " + comName + ". " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok(new Gson().toJson(model)).build();
    }

    /**
     * Get all Contact models from database
     * @return A list of Account Models in json
     */
    @GET
    @Path("account/all")
    public Response getAllAccounts() {
        List<AccountModel> list = new ArrayList<AccountModel>();

        try {
            list = marketPlaceService.getAccounts();
        } catch (Exception e) {
            String errMsg = "Error retrieving all Accounts. " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok(new Gson().toJson(list)).build();
    }

    /**
     * Update or create a persisted Account record in the database.
     * @param model AccountModel
     * @return AccountModel in json
     */
    @POST
    @Path("account/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveAccount(AccountModel model){

        try {
            marketPlaceService.saveAccount(model);
        }catch (Exception e){
            String errMsg = "Error saving/updating Account. " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok("Account added/updated").build();
    }

    /**
     * Delete a single Account record from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     */
    @DELETE
    @Path("account/delete/{ID}")
    public Response deleteAccount(@PathParam("ID") int ID){

        try{
            marketPlaceService.deleteAccount(ID);
        } catch (Exception e){
            String errMsg = "Error deleting Account. " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok("Account deleted").build();
    }

    /**
     * Get all contacts for an account by account PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return A list of ContactModels in json
     */
    @GET
    @Path("contacts/for/account/{ID}")
    public Response getContactsForAccount(@PathParam("ID") int ID) {
        List<ContactModel> list = new ArrayList<ContactModel>();

        try {
            list = marketPlaceService.getContactsForAccount(ID);
        } catch (Exception e) {
            String errMsg = "Error retrieving all Contacts for Account with ID " + ID + ". " + e.getMessage();
            log.error(errMsg);
            return Response.serverError().entity(errMsg).build();
        }

        return Response.ok(new Gson().toJson(list)).build();
    }
}

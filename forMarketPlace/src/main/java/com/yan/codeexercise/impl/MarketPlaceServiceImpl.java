package com.yan.codeexercise.impl;

import com.yan.codeexercise.api.MarketPlaceService;
import com.yan.codeexercise.common.DataAccessObjectException;
import com.yan.codeexercise.dao.MarketPlaceDaoService;
import com.yan.codeexercise.dao.ao.Account;
import com.yan.codeexercise.dao.ao.MarketPlaceAOService;
import com.yan.codeexercise.model.AccountModel;
import com.yan.codeexercise.model.ContactModel;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class MarketPlaceServiceImpl implements MarketPlaceService {
    private MarketPlaceDaoService marketPlaceDaoService;

    @Inject
    public MarketPlaceServiceImpl(MarketPlaceAOService marketPlaceAOService) {
        this.marketPlaceDaoService = marketPlaceAOService;
    }

    /**
     *  Get a Contact model from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    public ContactModel getContact(int ID) throws DataAccessObjectException {
        return marketPlaceDaoService.getContact(ID);
    }

    /**
     *  Get a Contact model from database by Email
     * @param email Contact's email. Here I consider email is an unique identifier for contact (it is not necessary).
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    public ContactModel getContactByEmail(String email) throws DataAccessObjectException {
        return marketPlaceDaoService.getContactByEmail(email);
    }

    /**
     *  Get all Contact models from database
     * @return A list of Contact Models
     * @throws DataAccessObjectException
     */
    public List<ContactModel> getContacts() throws DataAccessObjectException {
        return marketPlaceDaoService.getContacts();
    }

    /**
     *  Update or create a persisted Contact record in the database.
     *  You can also use this method to associate a contact to an account
     * @param model  ContactModel
     * @return ContactModel
     * @throws DataAccessObjectException
     */
    public ContactModel saveContact(ContactModel model) throws DataAccessObjectException {
        return marketPlaceDaoService.saveContact(model);
    }

    /**
     * delete a single Contact record from database by PK
     * @param ID The unique entity identifier for the persisted Contact record.
     */
    public void deleteContact(int ID) {
        marketPlaceDaoService.deleteContact(ID);
    }

    /**
     *  Get an Account model from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return AccountModel
     * @throws DataAccessObjectException
     */
    public AccountModel getAccount(int ID) throws DataAccessObjectException {
        return marketPlaceDaoService.getAccount(ID);
    }

    /**
     *  Get an Account model from database by Company Name
     * @param companyName Account's company name. Here I consider company name is an unique identifier for contact (it is not necessary).
     * @return AccountModel
     * @throws DataAccessObjectException
     */
    public AccountModel getAccountByCompanyName(String companyName) throws DataAccessObjectException {
        return marketPlaceDaoService.getAccountByCompanyName(companyName);
    }

    /**
     * Get all Contact models from database
     * @return A list of Account Models
     * @throws DataAccessObjectException
     */
    public List<AccountModel> getAccounts() throws DataAccessObjectException {
        return marketPlaceDaoService.getAccounts();
    }

    /**
     *  Update or create a persisted Account record in the database.
     * @param model AccountModel
     * @return AccountModel
     */
    public AccountModel saveAccount(AccountModel model){
        return marketPlaceDaoService.saveAccount(model);
    }

    /**
     * delete a single Account record from database by PK
     * @param ID The unique entity identifier for the persisted Account record.
     */
    public void deleteAccount(int ID) {
        marketPlaceDaoService.deleteAccount(ID);
    }

    /**
     * Get all contacts for an account by account PK
     * @param ID The unique entity identifier for the persisted Account record.
     * @return A list of ContactModels
     * @throws DataAccessObjectException
     */
    public List<ContactModel> getContactsForAccount(int ID) throws DataAccessObjectException {
        return marketPlaceDaoService.getContactsForAccount(ID);
    }
}

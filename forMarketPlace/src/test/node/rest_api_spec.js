const frisby = require('frisby');

// jasmine-node --config url http://localhost:2990/jira --config user admin --config pass admin rest_api_spec.js

const url = (process.env['url']? process.env['url'] : "http://localhost:2990/jira");
const username = (process.env['user']? process.env['user'] : "admin");
const password = (process.env['pass']? process.env['pass'] : "admin");
const auth_header = { 'Authorization' : 'Basic ' + new Buffer(username + ':' + password).toString('base64')};
const xsrf_header = { 'X-Atlassian-Token' : 'no-check' };
const save_account_path = url + "/rest/marketplace/latest/common/account/save";
const get_all_accounts_path = url + "/rest/marketplace/latest/common/account/all";

const save_account_payload = {
    "id": -1,
    "companyName": "Atlassian",
    "addressLine1": "1111 congress",
    "addressLine2": "",
    "city": "Austin",
    "state": "TX",
    "postalCode": "78703",
    "country": "US"
};

frisby.globalSetup({
    request: {
        headers: auth_header
    }
});

frisby.create('Save Account')
    .post(save_account_path, save_account_payload, {json: true})
    .addHeaders(xsrf_header)
    .inspectRequest()
    .inspectStatus()
    .expectStatus(200)
    .toss();

frisby.create('Get Modified Targets')
    .get(get_all_accounts_path)
    .inspectRequest()
    .inspectStatus()
    .expectJSONTypes('*', {
        id: Number,
        companyName: String,
        addressLine1: String,
        addressLine2: String,
        city: String,
        state: String,
        postalCode: String,
        country: String
    })
    .inspectJSON()
    .expectStatus(200)
    .afterJSON(function(body) {
        const delete_account_path = url + "/rest/marketplace/latest/common/account/delete/" + body[0].id;

        frisby.create("Delete account")
            .delete(delete_account_path)
            .inspectRequest()
            .inspectStatus()
            .expectStatus(200)
            .toss();
    })
    .toss();




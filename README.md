# Altas-exercise

## What is the exercise
### Technical requirements
* Create an application that can handle web requests
* The web server should provide a REST API for the following objects and lists of objects in application/JSON
* Contact (GET/POST/PUT)
* Account (GET/POST/PUT)
* Account (GET)
	* Contacts
* The objects should be stored in a database

### Contacts should have the following attributes:
* Name
* Email address
* Address line 1
* Address line 2
* City
* State
* Postal code
* Country
* Accounts should have the following attributes:

### Company name
* Address line 1
* Address line 2
* City
* State
* Postal code
* Country

The account-contact relationship is one to many, and contacts may not necessarily be associated with an account.

## Dependency Installation
1. Java 8 (It is required for ASDK)
2. ASDK 6.3.10 (This is the version I used, other versions should be fine. How to install ASDK)
3. Install Node & npm (This is for API test)
4. After installing node, install Jasmine-node. You can install it by  running “sudo npm install jasmine-node -g” in terminal

## Start the application
1. Git clone/download the repo
2. In terminal, go into atlas-exercise/forMarketPlace directory.
3. Run “atlas-run” and this may take several minutes.
4. Go to http://localhost:2990/jira in browser. If the jira instance is started, then the app is started. There’s no need to setup the jira instance for playing with the app.

## Application structure and concept
For the exercise, I chose JAVA with ASDK. The application is a JIRA plugin, although it only has the server side.

There are 4 parts for the application.

1. Persistent layer. Persistent layer is in the “dao” directory. Here I use Active Object as my data storage. All Active Object specific stuff is in “ao” directory. “MarketPlaceDaoService” is an interface. It is also a persist adapter, so if someone wants to use postgres, oracle, or dynamo other than AO, he only needs to replace the “ao” directory with the new database services.

2. Service layer. Between the persistent layer and REST API, I add the service layer with interface “MarketPlaceService” and implementation “MarketPlaceServiceImpl”

3. REST APIs. All REST APIs are located in “rest/RestResource”. All the APIs will call the service layer. I used GSON and codehaus.jackson for serialization and deserialization. If the output of the api is the object, it is in json format

4. Test. I first wrote unit test in java to test my REST APIs by using RestAssured, but it didn’t work. In ASDK, the unit test happens before even the application is started, so it always fail. Then I use frisby.js to write test cases to test the REST.

Overall, I considered if I migrate the app to AWS serverless , persistent layer can be dynamo or other DB, and I can put service layer in lambda and REST layer in API Gateway. 

## How to run test cases
1. In a new terminal tab, go to directory: altas-exercise/forMarketPlace/src/test/node
2. Run “npm install”
3. Run “jasmine-node rest_api_spec.js”
4. You should see 3 tests passed and test details (see screenshot)
5. In case you have different base url, username and password, you can use this command to run the tests.
	* jasmine-node --config url http://localhost:2990/jira --config user admin --config pass admin rest_api_spec.js

## How to run curl script
1. In a new tab go to “altas-exercise/script”
2. Run “sh curl_script_name parameter” or “sh curl_script_name”
3. Some curl script needs parameter, some not.
4. accountExample.json and contactExample.json are example payloads. You may need to modify them when you play with curl scripts.




